// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    console.log(context.data);
    
    if(context.data.year < 1885 || context.data.year > 2020){
      return Promise.reject(new Error('Invalid Year'));
    }
    if(context.data.miles <= 0){
      return Promise.reject(new Error('Invalid Mileage'));
    }
    //I am unsure about these last two. I think i am testing if the string can be made into a substring but i am unsure if that is waht you want
    if(!context.data.make || !context.data.make.substring(0,30)){
      return Promise.reject(new Error('Invalid Make'));
    }
    if(!context.data.model || !context.data.model.substring(0,30)){
      return Promise.reject(new Error('Invalid Model'));
    }
    return context;
  };
}
