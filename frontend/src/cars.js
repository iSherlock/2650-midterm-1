import React, { Component } from 'react';

class Cars extends Component {

  deleteCar(id, ev) {
    // HERE IS YOUR HANDLE TO THE BACKEND CAR SERVICE
    // eslint-disable-next-line
    const { carService } = this.props;

    /////////////////////////////////////////////////
    // STEP 15
    // WRITE THE CODE HERE TO REMOVE THE SELECTED CAR
    // START v
    /////////////////////////////////////////////////
    carService.remove(id);

    
    /////////////////////////////////////////////////
    // END ^
    /////////////////////////////////////////////////
  }
  
  addCar(ev) {
    // HERE IS YOUR HANDLE TO THE BACKEND CAR SERVICE
    // eslint-disable-next-line
    const { carService } = this.props;
    
    ///////////////////////////////////////
    // STEP 12
    // WRITE THE CODE HERE TO ADD A NEW CAR
    // START v
    ///////////////////////////////////////
    const inputMake = ev.target.querySelector('[id="make"]');
    const make = inputMake.value.trim();
    const inputModel = ev.target.querySelector('[id="model"]');
    const model = inputModel.value.trim();
    const inputYear = ev.target.querySelector('[id="year"]');
    const year = inputYear.value.trim();
    const inputMiles = ev.target.querySelector('[id="mileage"]');
    const miles = inputMiles.value.trim();
    console.log(make);
    console.log(model);
    console.log(year);
    console.log(miles);
    
    carService.create({
      make,
      model,
      year,
      miles
    })
    .then(() => {
      inputMake.value = '';
      inputModel.value = '';
      inputYear.value = '2020';
      inputYear.value = '0';
    })
    .catch(error => console.log(error.message));

    ///////////////////////////////////////
    // END ^
    ///////////////////////////////////////
    
    ev.preventDefault();
  }

  render() {
    const { cars } = this.props;

    return(
    <div>
      <div className="py-5 text-center">
        <h2>Cars</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addCar.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-4 mb-3">
                <label htmlFor="make">Make</label>
                <input type="text" className="form-control" id="make" defaultValue="" required />
                <div className="invalid-feedback">
                    A car make is required.
                </div>
              </div>

              <div className="col-md-4 mb-3">
                <label htmlFor="model">Model</label>
                <input type="text" className="form-control" id="model" defaultValue="" required />
                <div className="invalid-feedback">
                    A car model is required.
                </div>
              </div>

              <div className="col-md-4 mb-3">
                <label htmlFor="year">Year</label>
                <input type="number" className="form-control" id="year" defaultValue="2020" required />
                <div className="invalid-feedback">
                    A model year is required.
                </div>
              </div>
              
              <div className="col-md-4 mb-3">
                <label htmlFor="mileage">Mileage</label>
                <input type="number" className="form-control" id="mileage" defaultValue="0" required />
                <div className="invalid-feedback">
                    A model year is required.
                </div>
              </div>

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add car</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Make</th>
            <th scope="col">Model</th>
            <th scope="col">Year</th>
            <th scope="col">Mileage</th>
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody>

          {cars && cars.map(car => <tr key={car.id}>
            <th scope="row">{car.id}</th>
            <td>{car.make}</td>
            <td>{car.model}</td>
            <td>{car.year}</td>
            <td>{car.miles}</td>
            <td><button onClick={this.deleteCar.bind(this, car.id)} type="button" className="btn btn-danger">Delete</button></td>
          </tr>)}

        </tbody>
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Cars;
